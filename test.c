#include <stdio.h>

int i=0;

struct Node_struct
{
    int data;
    struct Node_struct* next;  
};

typedef struct Node_struct node;

void printList(node *head)
{
    node *tmp = head;
    while (tmp != NULL) 
    {
        printf("%d - ", tmp->data);
        tmp=tmp->next;
    }
    printf("\n");
    
    return;
}

node *create_new_node(int value)
{
    node *result = malloc(sizeof(node));
    result->data = value;
    result->next = NULL;
    return result;
}

int main()
{
    node n1, n2, n3, n4; //3 zmienne strukturalne typu node
    node *head = NULL; // wskaźnik na początek listy
    node *tmp;
    

    for (i=0; i<25; i++)
    {
        tmp = create_new_node(i);
        tmp->next = head;
        head = tmp;
    }
  

    printList(head);

/* 
    n1.data = 9;
    n2.data = 0;
    n3.data = 1; 

    //łączenie w lstę: najperw n1 potem n2 potem n3

    head = &n1; //głowa wskazuje na n1
    n1.next = &n2; //n1 wskazuje na n2
    n2.next = &n3; //n2 wskazuje na n3
    n3.next = NULL; //n3 wskazuje na n1
    printList(head);

    //usuwam n2 - czyli łączę n1 z n3
    head = &n1;
    n1.next = &n3;
    n3.next = NULL;
    printList(head);

    //dodam n4
    n4.data = 102;
    head = &n1; //głowa wskazuje na n1
    n1.next = &n2; //n1 wskazuje na n2
    n2.next = &n3; //n2 wskazuje na n3
    n3.next = &n4;
    n4.next = NULL; //n3 wskazuje na n1
    printList(head);

    //przeniosę n2 na koniec
    head = &n1;
    n1.next = &n3;
    n3.next = &n4;
    n4.next = &n2;
    n2.next = NULL;
    printList(head);
 */

    return 0;
}