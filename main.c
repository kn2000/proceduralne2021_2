#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

void freeList(struct Client *head)
    {
     struct Client *tmp;

     while (head != NULL)
     {
       tmp = head;
       head = head->next;
       free(tmp);
     }
       return;
    }

int main()
{

    int choice = 0;
    int numOfClients = 0;
    int checker = 0;
    char surname[30] = " ";
    char *p_surname;
    int isItFirst = 0;
    
    //head definition
    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client));
    head = NULL;

    //choice of action
    do
    {
        printf("\n\tChoose action:\n \t- Add a client[1]\n \t- Remove a client[2]\n \t- Display number of clients[3]\n \t- Display the list[4]\n \t- Exit[5]\n\n");
        scanf("%d", &choice);
 
        switch(choice)
        {
            case 1:

            printf("\nNumber of clients: ");
            scanf("%d", &numOfClients);

                for (int i = 0; i < numOfClients; i++)
                {
                    printf("%dst client's surname: ", i+1);
                    scanf("%s", surname);
                    p_surname = &surname;
                    push_back(&head, p_surname);
                } 
                printf("\n\tMain menu[0]\n\tExit[any digit]\n");
                scanf("%d", &checker);
                break;
            case 2:
                if(head==NULL)
                {
                    printf("Firstly you must insert surnames!\n");
                    checker = 0;
                }
                else
                {
                    printf("Warning! You can only delete surnames that are already on the list.\n\n");
                    //struct Client *somebody = *head;
                    printf("Surname: ");
                    scanf("%s", surname);
                    p_surname = &surname;

                    if(strcmp(p_surname,head->surname)==0)
                        {
                            pop_front(&head);
                        }
                    else
                    {
                        pop_by_surname(&head, p_surname);
                    }
                    
                    printf("\n\tMain menu[0]\n\tExit[any digit]\n");
                    scanf("%d", &checker);
                }
                break;
            case 3:
                printf("\nNumber of clients: %d", list_size(head));
                printf("\n\n\tMain menu[0]\n\tExit[any digit]\n");
                scanf("%d", &checker);
                break;
            case 4:
                show_list(head);
                printf("\n\tMain menu[0]\n\tExit[any digit]\n");
                scanf("%d", &checker);
                break;
            case 5:
                return 0;
                break;
        }
    }while(checker == 0);

    return 0;
}
